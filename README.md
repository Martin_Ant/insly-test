# Insly test assignment

## Install

With docker - everything would work with 2 commands.
```bash
docker create network insly_gateway
docker-compose up -d
```

Or setup you webserver to serve [./app/public/index.php](app/public/index.php)

If you want to play around and see file changes, then start then use `docker-compose -f ./docker-compose.dev.yml up -d` 
and run `composer install` either locally or inside the container `docker exec -i insly_app_1 composer install`

## Usage
Assuming you used docker for installation you an now go navigate to http:://insly.localhost:82

If for whatever reason port 82 is used, then you can modify `docker-compose.yml` proxy section and mount the proxy service to different port.

## Task1
Read the header. Code in [./app/view/calculator.php](app/view/calculator.php).
Didn't really understand what was requested, so technically it does what was asked.

## Task 2
Everything you see...

## Task 3
Check in [./task3](task3/) folder.

Assuming your active directory is project root:

Import the schema
`docker exec -i insly_db_1 mysql -uroot -proot insly_test < ./task3/schema.sql`

Import data
`docker exec -i insly_db_1 mysql -uroot -proot insly_test < ./task3/data.sql`

Check the select SQLs `./task3/query-example.sql`;

Log into mysql:
`docker exec -it insly_db_1 mysql -uroot -proot`

Or log in with you DB Manager, server is mounted to `localhost:3312`

Skipped the "Log info" assignment because if you meant to solve it using only SQL, then I don't know how to solve this without some extensive research.
If it was meant to be solved using PHP then that is too boilerplate code to write, especially considering that one of the requirements was to use vanilla PHP. 