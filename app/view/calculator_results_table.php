<?php
/**
 * @var int $instalments
 * @var array $results
 */
?>
<table class="table is-fullwidth is-striped is-hoverable">
    <thead>
    <tr>
        <th></th>
        <th>Policy</th>
        <?php for ($i=1; $i <= $instalments; $i++): ?>
            <th><?php echo sprintf('%s instalment', $i); ?> </th>
        <?php endfor; ?>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($results as $result): ?>
        <tr>
            <td style="font-weight: bold"><?php print $result['label']; ?></td>
            <td><?php print $result['amount']; ?></td>
            <?php for ($i=1; $i <= $instalments; $i++): ?>
                <td><?php print $result['instalment']; ?></td>
            <?php endfor; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>