<html>
<head>
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
    <script defer
            src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <script type="text/javascript" src="/assets/js/calculator.js"></script>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function (event) {
            const calc = new Calculator(document.getElementById('calculator'))
        })
    </script>
    <title>Insly Test</title>
</head>
<body>

<section class="hero is-primary">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                Insurance calculator
            </h1>
            <h2 class="subtitle">
                <!-- #Task 1 -->
                <?php foreach (['T', 'a', 's', 'k', '#', '1', ' ', 'i', 's', ' ', 'p', 'o', 'i', 'n', 't', 'l', 'e', 's', 's', '!'] as $letter): ?><?php echo $letter; ?><?php endforeach; ?>
            </h2>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div id="message-area"></div>
        <form id="calculator">
            <div class="field ">
                <label class="label">Estimated value of the car (EUR)</label>

                <p class="control">
                    <input class="input" type="text" name="est-val" id="est-val"
                           placeholder="Estimated value of the car">
                </p>

                <p class="help">A value between 100 - 100 000 EUR</p>
            </div>

            <div class="field ">
                <label class="label">Tax percentage</label>

                <p class="control">
                    <input class="input" type="number" name="tax" min="0"
                           max="100"
                           placeholder="Tax percentage"
                           id='tax'
                    >
                </p>

                <p class="help">A value between 0 - 100</p>
            </div>

            <div class="field ">
                <label class="label">Number of instalments</label>

                <p class="control">
                    <input class="input" type="number" name="instalments" min="0"
                           max="12"
                           placeholder="Number of instalments"
                           id="instalments"
                    >
                </p>

                <p class="help">1 - 12 instalments</p>
            </div>

            <input class="button" type="submit" value="Calculate">
        </form>

        <div id="calculator-results"></div>
    </div>
</section>
</body>
</html>