<?php

require "../vendor/autoload.php";

use App\Request;
use App\Response;
use App\Router;

error_reporting(E_ALL);

define('ROOT_DIR', realpath(dirname(__FILE__ ) . '/..'));

try {
    $response = Router::resolve(
        Request::make()
    );
} catch (Throwable $e) {
    $response = new Response('Internal server error', 500);
}

$response->send();
