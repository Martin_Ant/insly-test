const API_ENDPOINT = '/calculator';

function Calculator (form) {
  this.form = form;
  this.messageArea = document.getElementById('message-area');
  this.resultsArea = document.getElementById('calculator-results');

  this.clearMessages = () => {
    this.messageArea.innerHTML = '';
  }

  this.addMessage = (msg, type) => {

    let msgClass = ''

    if (typeof type !== 'undefined') {
      msgClass = 'is-' + type
    }

    let elem = document.createElement('div');

    elem.innerHTML =
      '<article class="message ' + msgClass + '" style="margin-bottom: 1em">\n' +
      '  <div class="message-body">\n' +
      msg +
      '  </div>\n' +
      '</article>';

    this.messageArea.appendChild(elem);
  }

  this.handleSubmit = (e) => {
    e.preventDefault();
    this.clearMessages();
    let formData = new FormData(this.form);
    let oReq = new XMLHttpRequest();
    const _this = this;
    oReq.open("POST", API_ENDPOINT, true);
    oReq.enctype = 'multipart/form-data';

    oReq.onload = function(oEvent) {
      if (oReq.status === 200) {
        let response = JSON.parse(oReq.response);
        _this.addMessage("Calculated!", 'success');
        _this.resultsArea.innerHTML = response.data;
      } else if (oReq.status === 400) {
        let response = JSON.parse(oReq.response);
        _this.addMessage(response.error, 'danger');
      } else {
        _this.addMessage("Server error HTTP " + oReq.status + " occurred.", 'danger');
      }
    };

    oReq.send(formData);
  }

  this.form.addEventListener('submit', this.handleSubmit.bind(this))
}

