<?php

namespace App;

class View
{
    private $args;

    public function __construct(array $args = [])
    {
        $this->args = $args;
    }

    public function render(string $template)
    {
        $path = self::getTemplatePath($template);

        ob_start();
        foreach ($this->args as $name => $val) {
            ${$name} = $val;
        }

        include "$path";
        return ob_get_clean();
    }

    /**
     * @param string $template
     *
     * @return string
     * @throws \Exception
     */
    private static function getTemplatePath(string $template)
    {
        $path = sprintf('%s/%s.php', self::getTemplateDirectory(), $template);

        if (!file_exists($path)) {
            throw new \Exception(sprintf('Template %s not found on path %s', $template,$path));
        }

        return $path;
    }

    private static function getTemplateDirectory()
    {
        return ROOT_DIR . '/view';
    }
}