<?php

namespace App\Controller;

use App\JsonResponse;
use App\Response;
use App\Service\Calculator\Calculator;
use App\Service\Calculator\InsurancePolicy;
use App\Service\Calculator\ValidationException;
use App\View;

class CalculatorController extends BaseController
{
    public function index()
    {
        $view = new View();
        return new Response($view->render('calculator'));
    }

    public function calculate()
    {
        $code = 200;

        try {

            self::validate();

            $estimatedValue = $_POST['est-val'];
            $tax =  $_POST['tax'];
            $instalments = $_POST['instalments'];
            $calculator = new Calculator();

            $view = new View([
                'results' => $calculator->calculate($estimatedValue, $tax, $instalments),
                'instalments' => $instalments,
            ]);

            $result['data'] = $view->render('calculator_results_table');
        } catch (ValidationException $e) {
            $result = [
              'error' => $e->getMessage()
            ];

            $code = 400; // Bad request.
        }

        return new JsonResponse($result, $code);
    }

    /**
     * @throws ValidationException
     */
    private static function validate()
    {
        if (empty($_POST['est-val'])) {
            throw new ValidationException('Missing estimated value');
        }

        if (empty($_POST['tax'])) {
            throw new ValidationException('Missing tax value');
        }

        if (empty($_POST['instalments'])) {
            throw new ValidationException('Missing instalment value');
        }

        if (!is_numeric($_POST['est-val'])) {
            throw new ValidationException('Estimated value must be numeric');
        }

        if (!is_numeric($_POST['tax'])) {
            throw new ValidationException('Tax value must be numeric');
        }

        if (!is_numeric($_POST['instalments'])) {
            throw new ValidationException('Instalments value must be numeric');
        }
    }

}