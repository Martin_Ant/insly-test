<?php

namespace App\Controller;

use App\Request;

abstract class BaseController
{

    /**
     * @var \App\Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    protected function getRequest(): Request
    {
        return $this->request;
    }
}