<?php

namespace App;

class JsonResponse extends Response
{

    protected function sendContent()
    {
        print json_encode($this->result);
    }

    protected function getHeaders(): array
    {
        return [
          'Content-Type: application/json'
        ];
    }
}