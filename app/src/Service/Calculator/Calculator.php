<?php

namespace App\Service\Calculator;

class Calculator
{

    private static $minEstimatedVal = 100;

    private static $maxEstimatedVal = 100000;

    private static $minInstalments = 1;

    private static $maxInstalments = 12;

    const ROUNDING_PRECISION = 2;

    /**
     * @param float $estimatedValue
     * @param float $tax
     * @param int $instalments
     *
     * @return array
     * @throws \App\Service\Calculator\ValidationException
     */
    public function calculate(
        float $estimatedValue,
        float $tax,
        int $instalments
    ) {
        self::validateEstimatedValue($estimatedValue);
        self::validateInstalments($instalments);
        $policy = new InsurancePolicy($estimatedValue, $tax,
            $this->getBasePricePercentage(), $this->getCommissionPercentage());

        $result[] = [
            'label' => 'Value',
            'amount' => $estimatedValue,
            'instalment' => null,
        ];
        $total = 0;

        foreach ($policy->getRows() as $row) {
            $val = round($row->getValue(), self::ROUNDING_PRECISION);
            $total += $val;

            $result[] = [
                'label' => $row->getLabel(),
                'amount' => $val,
                'instalment' => self::formatNumber($val / $instalments),
            ];
        }

        $result[] = [
            'label' => 'Total cost',
            'amount' => self::formatNumber($total),
            'instalment' => self::formatNumber($total / $instalments),
        ];

        return $result;
    }

    private static function formatNumber(float $num)
    {
        return number_format(round($num, self::ROUNDING_PRECISION), 2);
    }

    /**
     * @param float $val
     *
     * @throws \App\Service\Calculator\ValidationException
     */
    private static function validateEstimatedValue(float $val)
    {
        if ($val < self::$minEstimatedVal) {
            throw new ValidationException(sprintf('Estimated value has to be more than %s EUR',
                self::$minEstimatedVal));
        }

        if ($val > self::$maxEstimatedVal) {
            throw new ValidationException(sprintf('Estimated value must not exceed %s EUR',
                self::$maxEstimatedVal));
        }
    }

    /**
     * @param int $instalments
     *
     * @throws \App\Service\Calculator\ValidationException
     */
    private static function validateInstalments(int $instalments)
    {
        if ($instalments > self::$maxInstalments) {
            throw new ValidationException(sprintf('Maximum of %s instalments allowed',
                self::$maxInstalments));
        }

        if ($instalments < self::$minInstalments) {
            throw new ValidationException(sprintf('There has to at least %s instalments',
                self::$minInstalments));
        }
    }

    private function getBasePricePercentage(): float
    {
        if (!isset($this->percentage)) {

            if (date('D') === 'Fri' && date('H') >= 15 && date('H') <= 20) {
                $this->percentage = 13;
            } else {
                $this->percentage = 11;
            }

        }

        return $this->percentage;
    }

    private function getCommissionPercentage(): float
    {
        return 17;
    }
}