<?php

namespace App\Service\Calculator\PolicyRows;

use App\Service\Calculator\InsurancePolicy;

class BasePremiumRow implements PolicyRowInterface
{

    /** @var InsurancePolicy  */
    private $policy;

    /**
     * BasePremiumRow constructor.
     * @param InsurancePolicy $policy
     */
    public function __construct(InsurancePolicy $policy)
    {
        $this->policy = $policy;
    }

    /**
     * @return InsurancePolicy
     */
    public function getPolicy(): InsurancePolicy
    {
        return $this->policy;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return sprintf('Base premium (%s%%)', $this->getPercentage());
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return ($this->getPolicy()
              ->getInsuredValue() / 100) * $this->getPercentage();
    }

    /**
     * @return float
     */
    public function getPercentage(): float
    {
        return $this->getPolicy()->getBasePricePercentage();
    }
}