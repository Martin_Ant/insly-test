<?php

namespace App\Service\Calculator\PolicyRows;

use App\Service\Calculator\InsurancePolicy;

abstract class BasePolicyRow implements PolicyRowInterface
{

    private $policy;

    public function __construct(InsurancePolicy $policy)
    {
        $this->policy = $policy;
    }

    protected function getPolicy(): InsurancePolicy
    {
        return $this->policy;
    }

    public abstract function getLabel(): string;

    public abstract function getValue(): float;
}