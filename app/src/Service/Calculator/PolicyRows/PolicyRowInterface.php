<?php

namespace App\Service\Calculator\PolicyRows;

interface PolicyRowInterface
{
    public function getLabel(): string;

    public function getValue(): float;
}