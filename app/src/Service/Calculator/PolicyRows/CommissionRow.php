<?php

namespace App\Service\Calculator\PolicyRows;

class CommissionRow implements PolicyRowInterface
{

    /**
     * @var \App\Service\Calculator\PolicyRows\BasePremiumRow
     */
    private $basePremiumRow;

    public function __construct(BasePremiumRow $basePremiumRow)
    {
        $this->basePremiumRow = $basePremiumRow;
    }

    public function getPercentage(): float
    {
        return $this->basePremiumRow->getPolicy()->getCommissionPercentage();
    }

    public function getLabel(): string
    {
        return sprintf('Commission (%s%%)', $this->getPercentage());
    }

    public function getValue(): float
    {
        return ($this->basePremiumRow->getValue() / 100) * $this->getPercentage();
    }
}