<?php

namespace App\Service\Calculator;

use App\Service\Calculator\PolicyRows\BasePremiumRow;
use App\Service\Calculator\PolicyRows\CommissionRow;
use App\Service\Calculator\PolicyRows\TaxRow;

class InsurancePolicy
{
    /**
     * @var float
     */
    private $insuredValue;

    private static $minEstimatedVal = 100;

    private static $maxEstimatedVal = 100000;

    /**
     * @var float
     */
    private $taxPercentage;

    /**
     * @var float
     */
    private $basePricePercentage;

    /**
     * @var float
     */
    private $commissionPercentage;

    /** @var \App\Service\Calculator\PolicyRows\PolicyRowInterface[] */
    private $rows = [];

    /**
     * Calculator constructor.
     *
     * @param float $insuredValue
     * @param float $taxPercentage
     * @param float $basePricePercentage
     * @param float $commissionPercentage
     *
     * @throws \App\Service\Calculator\ValidationException
     */
    public function __construct(
        float $insuredValue,
        float $taxPercentage,
        float $basePricePercentage,
        float $commissionPercentage
    ) {
        $this->insuredValue = $insuredValue;
        $this->taxPercentage = $taxPercentage;
        $this->basePricePercentage = $basePricePercentage;
        $this->commissionPercentage = $commissionPercentage;

        $this->validate();
        $this->buildPolicy();
    }

    /**
     * @return float
     */
    public function getTaxPercentage(): float
    {
        return $this->taxPercentage;
    }

    /**
     * @return float
     */
    public function getInsuredValue(): float
    {
        return $this->insuredValue;
    }

    public function getTotalCost(): float
    {
        $total = 0;

        foreach ($this->rows as $row) {
            $total += $row->getValue();
        }

        return $total;
    }

    /**
     * @return float
     */
    public function getBasePricePercentage(): float
    {
        return $this->basePricePercentage;
    }

    /**
     * @return float
     */
    public function getCommissionPercentage(): float
    {
        return $this->commissionPercentage;
    }

    /**
     * @throws \App\Service\Calculator\ValidationException
     */
    protected function validate()
    {
        if ($this->insuredValue < self::$minEstimatedVal) {
            throw new ValidationException(sprintf('Estimated value has to be more than %s EUR', self::$minEstimatedVal));
        }

        if ($this->insuredValue > self::$maxEstimatedVal) {
            throw new ValidationException(sprintf('Estimated value can not exceed %s EUR', self::$maxEstimatedVal));
        }

        if ($this->taxPercentage > 100 || $this->taxPercentage < 0) {
            throw new ValidationException(sprintf('Tax percentage must be between %s and %s', 0, 100));
        }
    }

    /**
     * @return \App\Service\Calculator\PolicyRows\PolicyRowInterface[]
     */
    public function getRows()
    {
        return $this->rows;
    }

    private function buildPolicy()
    {
        $basePremiumRow = new BasePremiumRow($this);
        array_push($this->rows, $basePremiumRow);
        array_push($this->rows, new CommissionRow($basePremiumRow));
        array_push($this->rows, new TaxRow($basePremiumRow));
    }

}