<?php

namespace App;

class Response
{

    /** @var int */
    protected $httpCode;

    /** @var mixed */
    protected $result;

    /** @var string */
    protected $httpVersion = '1.0';

    public function __construct($content, int $httpCode = 200)
    {
        $this->httpCode = $httpCode;
        $this->result = $content;
    }

    protected function getHttpMessage()
    {
        return '';
    }

    protected function getHeaders()
    {
        return [
          'Content-Type: text/html'
        ];
    }

    protected function sendHeader()
    {
        header(sprintf("HTTP/%s %s %s", $this->httpVersion, $this->httpCode,
          $this->getHttpMessage()));

        foreach ($this->getHeaders() as $header) {
            header($header);
        }
    }

    protected function sendContent()
    {
        print $this->result;
    }

    public function send()
    {
        $this->sendHeader();
        $this->sendContent();
    }
}