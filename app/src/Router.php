<?php

namespace App;

use App\Controller\CalculatorController;

class Router
{

    public static function resolve(Request $request): Response
    {
        switch ($request->getUri()) {
            case '/':
                switch ($request->getMethod()) {
                    case 'GET':
                        return (new CalculatorController($request))
                            ->index();
                        break;
                    default:
                        return new Response(sprintf('Unsupported request method "%s"', $request->getMethod()), 405);
                }
                break;
            case '/calculator':

                switch ($request->getMethod()) {
                    case 'POST':
                        return (new CalculatorController($request))
                          ->calculate();
                        break;
                    default:
                        return new Response(sprintf('Unsupported request method "%s"', $request->getMethod()), 405);
                }

                break;
            default:
                return new Response('Page not found', 404);
        }
    }
}