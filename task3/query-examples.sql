
-- Data in English.
select e.*,
       i.introduction,
       we.start         as work_start,
       we.end           as work_end,
       wem.company_name as company,
       wem.title        as work_title,
       wem.description  as work_escription,
       ed.start         as education_start,
       ed.end           as education_end,
       em.school_name   as school,
       em.field_of_study,
       em.description   as study_description
from employee e
         left join introduction i on e.ssn = i.employee_ssn and i.language = 'en'
         left join work_experience we on e.ssn = we.employee_ssn
         left join education ed on e.ssn = ed.employee_ssn
         left join education_meta em on ed.id = em.education_id and em.language = 'en'
         left join work_experience_meta wem on we.id = wem.work_experience_id and wem.language = 'en'
where e.ssn = 39212170814;


-- Data in Spanish.
select e.*,
       i.introduction,
       we.start         as work_start,
       we.end           as work_end,
       wem.company_name as company,
       wem.title        as work_title,
       wem.description  as work_escription,
       ed.start         as education_start,
       ed.end           as education_end,
       em.school_name   as school,
       em.field_of_study,
       em.description   as study_description
from employee e
         left join introduction i on e.ssn = i.employee_ssn and i.language = 'es'
         left join work_experience we on e.ssn = we.employee_ssn
         left join education ed on e.ssn = ed.employee_ssn
         left join education_meta em on ed.id = em.education_id and em.language = 'es'
         left join work_experience_meta wem on we.id = wem.work_experience_id and wem.language = 'es'
where e.ssn = 39212170814;


-- Data in French.
select e.*,
       i.introduction,
       we.start         as work_start,
       we.end           as work_end,
       wem.company_name as company,
       wem.title        as work_title,
       wem.description  as work_escription,
       ed.start         as education_start,
       ed.end           as education_end,
       em.school_name   as school,
       em.field_of_study,
       em.description   as study_description
from employee e
         left join introduction i on e.ssn = i.employee_ssn and i.language = 'fr'
         left join work_experience we on e.ssn = we.employee_ssn
         left join education ed on e.ssn = ed.employee_ssn
         left join education_meta em on ed.id = em.education_id and em.language = 'fr'
         left join work_experience_meta wem on we.id = wem.work_experience_id and wem.language = 'fr'
where e.ssn = 39212170814;

