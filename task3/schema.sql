SET FOREIGN_KEY_CHECKS = 0;

drop table if exists `employee`;
create table `employee`
(
    name        varchar(255) not null,
    birthdate   datetime default null,
    ssn         bigint unsigned unique,
    is_employee bool     default 0
);

drop table if exists `introduction`;
create table `introduction`
(
    `employee_ssn` bigint unsigned,
    introduction   text    not null,
    language       char(2) not null,
    index `fk_employee_ssn` (`employee_ssn`),
    unique index `unique_intro_perLang` (`employee_ssn`, `language`),
    constraint `fk_employee_ssn`
        foreign key (`employee_ssn`)
            references `employee` (`ssn`)
            on update cascade
            on delete cascade
);

drop table if exists `work_experience`;
create table `work_experience`
(
    id           integer unsigned auto_increment,
    employee_ssn bigint unsigned,
    start        datetime not null,
    end          datetime default null,
    primary key (id),
    index `fk_employee_ssn_2` (`employee_ssn`),
    constraint `fk_employee_ssn_2`
        foreign key (`employee_ssn`)
            references `employee` (`ssn`)
            on update cascade
            on delete cascade
);

drop table if exists `work_experience_meta`;
create table `work_experience_meta`
(
    work_experience_id int unsigned not null,
    company_name       varchar(255) not null,
    title              varchar(255),
    description        text,
    language           char(2)      not null,
    index `fk_work_experience_id` (`work_experience_id`),
    unique index `exp_per_lang` (`work_experience_id`, language),
    constraint `fk_work_experience_id`
        foreign key (`work_experience_id`)
            references `work_experience` (`id`)
            on update cascade
            on delete cascade
);

drop table if exists `education`;
create table `education`
(
    id           integer unsigned auto_increment,
    employee_ssn bigint unsigned,
    start        datetime not null,
    end          datetime default null,
    primary key (id),
    index `fk_employee_ssn_3` (`employee_ssn`),
    constraint `fk_employee_ssn_3`
        foreign key (`employee_ssn`)
            references `employee` (`ssn`)
            on update cascade
            on delete cascade
);

drop table if exists `education_meta`;
create table `education_meta`
(
    education_id   int unsigned not null,
    school_name    varchar(255) not null,
    field_of_study varchar(255),
    description    text,
    language       char(2)      not null,
    index `fk_education_id` (`education_id`),
    unique index `education_per_lang` (`education_id`, language),
    constraint `fk_education_id`
        foreign key (`education_id`)
            references `education` (`id`)
            on update cascade
            on delete cascade
);

SET FOREIGN_KEY_CHECKS = 1;