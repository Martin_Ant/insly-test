-- Employee
insert into employee
values ('Randy Lahey', '1985-10-10', 39212170814, 1);

-- Intro
insert into `introduction`
values (39212170814, 'English intro', 'en');
insert into `introduction`
values (39212170814, 'Spanish intro', 'es');
insert into `introduction`
values (39212170814, 'French intro', 'fr');

-- Work XP
insert into work_experience (employee_ssn, start, end)
values (39212170814, '2000-01-01', '2010-01-01');

insert into work_experience (employee_ssn, start, end)
values (39212170814, '2010-01-01', '2013-01-01');

insert into work_experience_meta
values (1, 'English Company', 'English', 'English description', 'en');
insert into work_experience_meta
values (1, 'Spanish Company', 'Spanish job title', 'Spanish description', 'es');
insert into work_experience_meta
values (1, 'French Company', 'French  Job title', 'French description', 'fr');

insert into work_experience_meta
values (2, '222 English Company', '222 English', '222 English description', 'en');
insert into work_experience_meta
values (2, '222 Spanish Company', '222 Spanish job title', '222 Spanish description', 'es');
insert into work_experience_meta
values (2, '222 French Company', '222 French  Job title', '222 French description', 'fr');

-- Education
insert into education
values (null, 39212170814, '2015-01-01', '2018-01-01');

insert into education_meta
values (1, 'English School', 'English Field of study', 'English description', 'en');
insert into education_meta
values (1, 'Spanish School', 'Spanish Field of study', 'Spanish description', 'es');
insert into education_meta
values (1, 'French School', 'French Field of study', 'French description', 'fr');